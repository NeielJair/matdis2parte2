#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <errno.h>
#include "RomaVictor.h"
#include "list.h"
#include "GrafoSt21.h"
#include "UnleashHell.h"
#include "tree.h"
#include "util.h"

// Hecho por Neiel Urbano y Marcello Bussano

// Bit array
static void
bitarray_set(u32 a[], u32 i)
{
    a[i/32] |= (u32)1 << (i%32);
}

static u32
bitarray_test(u32 a[], u32 i)
{
    return (a[i/32] & ((u32)1 << (i%32))) != 0;
}

u32
Greedy(Grafo G)
{
    u32 n = NumeroDeVertices(G);
    u32 delta = Delta(G);
    u32 maxColor = 0;

    FijarColor(0, 0, G);
    for (u32 i = 1; i < n; i++)
        FijarColor((u32)(-1), i, G);

    u32 color;
    u32 degree;

    // Guardamos la información acerca de qué colores fueron usados en cada bit
    //de un u32. Accedemos a esta información con bitarray_{set,test}
    u32 usedColorsSize = (delta + 1) / 32 + 1;
    u32 *usedColors = malloc(usedColorsSize * sizeof(u32));

    for (u32 i = 1; i < n; i++) {
        memset(usedColors, 0, usedColorsSize * sizeof(u32));
        degree = Grado(i, G);

        for (u32 j = 0; j < degree; j++) {
            color = ColorVecino(j, i, G);
            if (color != (u32)(-1)) // color == -1 si no fue definido todavía
                bitarray_set(usedColors, color);
        }

        for (color = 0; color < delta + 1; color++) {
            if (!bitarray_test(usedColors, color))
                break;
        }

        FijarColor(color, i, G);
        if (color > maxColor)
            maxColor = color;
    }
    free(usedColors);
    return maxColor + 1;
}

char
Bipartito(Grafo G)
{
    u32 n = NumeroDeVertices(G);
    //printf("Checking if graph with %d vertexes is bipartite\n", n);
    for (u32 i = 0; i < n; i++)
        FijarColor((u32)(-1), i, G);

    List *queue = list_create();
    for (u32 i = 0; i < n; i++) {
        if (Color(i, G) == (u32)(-1)) {
            list_add(queue, i);
            FijarColor(0, i, G);

            u32 j, k, degree, color, neighborColor;
            while (!list_is_empty(queue)) {
                i = list_pop(queue);

                color = Color(i, G);
                degree = Grado(i, G);
                for (j = 0; j < degree; j++) {
                    // Iterate each neighbor
                    neighborColor = ColorVecino(j, i, G);
                    // Check if its color was already set
                    if (neighborColor == (u32)(-1)) {
                        // If it wasn't, color it opposite and add it to queue
                        k = OrdenVecino(j, i, G);
                        FijarColor(1 - color, k, G);
                        list_add(queue, k);
                    } else if (neighborColor == color) {
                        //Else if its color is the same as the current vertex's,
                        //the graph is not bipartite
                        goto not_bipartite;
                    }
                }
            }
        }
    }

    list_destroy(queue);
    //printf("\tDone\n");
    return 1;

    not_bipartite:
    list_destroy(queue);
    //printf("\tGraph is not bipartite, setting proper coloring.\n");
    Greedy(G);
    return 0;
}

char
AleatorizarVertices(Grafo G, u32 R)
{
    u32 n = NumeroDeVertices(G);
    srand(R);
    u32 offset = random_number(n - 1); // offset shouldn't be n
    u32 coprime = get_random_coprime(n);
    // j = (offset + coprime * i) % n is the random index
    //It will never point to a specific index more than once
    for (u32 i = 0; i < n; i++) {
        u32 j = ((uint64_t)coprime * i + offset) % n;
        FijarOrden(i, G, j);
    }
    return 0;
}

char
OrdenPorBloqueDeColores(Grafo G, u32 *perm)
{
    u32 n = NumeroDeVertices(G);

    // Get max color
    u32 maxColor = 0;
    for (u32 i = 0; i < n; i++) {
        u32 color = Color(i, G);
        if (color > maxColor)
            maxColor = color;
    }

    // Verify that perm is a permutation of 0..(maxColor-1)
    // Sort the contents of perm in a temporal array and check if
    //temp[i] = i, since the colors of the graph are continuous
    u32 colorAmount = maxColor + 1;
    u32 *temp = malloc(colorAmount * sizeof(u32));
    memcpy(temp, perm, colorAmount * sizeof(u32));
    qsort(temp, colorAmount, sizeof(u32), compare_u32);
    for (u32 i = 0; i < colorAmount; i++) {
        if (temp[i] != i) {
            free(temp);
            printf("\tperm is not a permutation\n");
            return 0;
        }
    }
    free(temp);

    // Reset graph order and populate tree
    Tree tree = tree_create(perm[0]);
    for (u32 i = 0; i < n; i++) {
        FijarOrden(i, G, i);
        tree_add(tree, Color(i, G), i);
    }

    // Set vertexes in order and set them in the graph
    List *list = tree_collapse(tree, perm, maxColor + 1);
    for (u32 i = 0; i < n; i++) {
        u32 j = list_pop(list);
        FijarOrden(i, G, j);
    }

    list_destroy(list);
    tree_destroy(tree);
    return 1;
}

u32
random_number(u32 max)
{
    return max * ((float)rand()/(float)RAND_MAX);
}

u32
get_gcd(u32 a, u32 b) {
    if (a == 0)
        return b;
    if (b == 0)
        return a;

    u32 x = a;
    u32 y = b;
    while (x != y) {
        if (x > y)
            x -= y;
        else
            y -= x;
    }

    return x;
}

u32
get_random_coprime(u32 x) {
    u32 ret = random_number(x - 2) + 1; // ret will never be 0 or x
    u32 gcd;
    while ((gcd = get_gcd(ret, x)) > 1)
        ret /= gcd;
    return ret;
}

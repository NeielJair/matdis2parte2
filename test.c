#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <errno.h>
#include "RomaVictor.h"
#include "GrafoSt21.h"
#include "UnleashHell.h"

#define max(a,b) \
  ({ __typeof__ (a) _a = (a); \
      __typeof__ (b) _b = (b); \
    _a > _b ? _a : _b; })

void
compare_graphs(Grafo g, Grafo h)
{
    printf("|g.V| = %d, |h.V| = %d\n", NumeroDeVertices(g), NumeroDeVertices(h));
    printf("|g.E| = %d, |h.E| = %d\n", NumeroDeLados(g), NumeroDeLados(h));
    printf(" g.Δ  = %d,  h.Δ  = %d\n\n", Delta(g), Delta(h));
    printf("\tg\t\t|\th\n");
    for (u32 i = 0; i < NumeroDeVertices(g); i++) {
        u32 deg_g = Grado(i, g);
        u32 deg_h = Grado(i, h);
        printf("Vertexes %d\n", i);
        printf("%d: %d %d\t\t~\t%d: %d %d\n",
            Nombre(i, g), Color(i, g), deg_g,
            Nombre(i, h), Color(i, h), deg_h);

        u32 max_deg = max(deg_g, deg_h);
        for (u32 j = 0; j < max_deg; j++) {
            if (j < deg_g)
                printf("\t-> %d: %d %d\t|\t\t",
                    NombreVecino(j, i, g), ColorVecino(j, i, g), OrdenVecino(j, i, g));
            else
                printf("\t->\t\t|\t\t");

            if (j < deg_h)
                printf("%d: %d %d\n",
                    NombreVecino(j, i, h), ColorVecino(j, i, h), OrdenVecino(j, i, h));
            else
                printf("\n");
        }
        printf("\n");
    }
}

void
try_greedy(Grafo g, u32 n)
{
    printf("Doing %d iterations\n", n);
    u32 minChi = Greedy(g);
    printf("\tGot chi %d\n", minChi);
    for (u32 i = 0; i < n - 1; i++) {
        AleatorizarVertices(g, i * rand());
        u32 chi = Greedy(g);
        printf("\tGot chi %d\n", chi);
        if (chi < minChi)
            minChi = chi;
    }
    printf("\tMin chi %d\n", minChi);
}

void
check_order(Grafo g) {
    u32 size = Greedy(g);
    u32 *perm = malloc(size * sizeof(u32));
    for (u32 i = 0; i < size; i++)
        perm[i] =  i;
    OrdenPorBloqueDeColores(g, perm);

    u32 n = NumeroDeVertices(g);
    u32 prev_color = -2;
    u32 color = -1;
    u32 count = 0;
    for (u32 i = 0; i < n; i++) {
        prev_color = color;
        color = Color(i, g);

        if (prev_color != color) {
            printf("%d had %d\n", prev_color, count+1);
            count = 0;
        } else
            count++;
    }

    free(perm);
}

int
main()
{
    Grafo g = ConstruccionDelGrafo();
    Grafo h = CopiarGrafo(g);
    AleatorizarVertices(h, 20);

    //compare_graphs(g, h);
    //try_greedy(g, 20);
    check_order(g);

    DestruccionDelGrafo(g);
    DestruccionDelGrafo(h);
    return 0;
}

#ifndef TREE_H
#define TREE_H

#include <stdint.h>
#include "list.h"

struct branch {
    uint32_t index;
    List *data;
    uint32_t size;
    struct branch *left;
    struct branch *right;
};

typedef struct branch *Tree;

Tree tree_create(uint32_t index);
void tree_add(Tree tree, uint32_t index, uint32_t elem);
List *tree_get(Tree tree, uint32_t index);
void tree_destroy(Tree tree);

List *tree_collapse(Tree tree, uint32_t *order, uint32_t orderSize);

#endif /* TREE_H */

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>

void
fail_if(char failCond, const char *msg)
{
    if (failCond) {
        printf("%s", msg);
        exit(0);
    }
}

int
compare_u32(const void *a, const void *b)
{
    uint32_t x = *(uint32_t*) a;
    uint32_t y = *(uint32_t*) b;
    if (x > y)
        return 1;
    else if (x < y)
        return -1;
    return 0;
}

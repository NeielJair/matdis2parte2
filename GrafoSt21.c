#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <errno.h>
#include "util.h"
#include "RomaVictor.h"
#include "list.h"

// Hecho por Neiel Urbano, Marcello Bussano y Eric Negreido.

#define BUFFER_SIZE 1024 // Largo máximo de las líneas a leer

// Vertex
static void
vertex_destroy(Vertex *vertex)
{
    if (vertex != NULL) {
        list_destroy(vertex->neighbors);
        free(vertex);
    }
}

static Vertex*
vertex_create(u32 name)
{
    Vertex *vertex = malloc(sizeof(Vertex));
    if (vertex == NULL)
        return NULL;

    vertex->name = name;
    vertex->degree = 0;
    vertex->color = 0;
    vertex->neighbors = list_create();
    return vertex;
}

static Vertex*
vertex_copy(Vertex *vertex)
{
    Vertex *new = vertex_create(vertex->name);
    new->degree = vertex->degree;
    new->color = vertex->color;
    free(new->neighbors);
    new->neighbors = list_copy(vertex->neighbors, new->degree);
    return new;
}

static void
vertex_add_neighbor(Vertex *vertex, u32 neighbor) //Returns last added neighbor node
{
    list_add(vertex->neighbors, neighbor);
    vertex->degree++;
}

// Edge
static Edge*
edge_create(u32 v1, u32 v2)
{
    Edge *edge = malloc(sizeof(Edge));
    if (edge == NULL)
        return NULL;

    edge->v1 = v1;
    edge->v2 = v2;
    edge->weight = 0;
    edge->sibling = NULL;
    return edge;
}

static void
edge_destroy(Edge *edge)
{
    if (edge != NULL)
        free(edge);
}

static Edge*
edge_copy(Edge *edge)
{
    Edge *e = edge_create(edge->v1, edge->v2);
    e->weight = edge->weight;
    return e;
}

//Helpers para el grafo
static Grafo
graph_create(u32 n_vertexes, u32 n_edges)
{
    Grafo graph = malloc(sizeof(GrafoSt));
    if (graph == NULL)
        return NULL;

    graph->n_vertexes = n_vertexes;
    graph->n_edges = n_edges;

    graph->vertexes = malloc(n_vertexes * sizeof(Vertex*));
    if (graph->vertexes == NULL) {
        DestruccionDelGrafo(graph); //Destruir lo que se hizo hasta ahora
        return NULL;
    }
    graph->edges = malloc(n_edges * sizeof(Edge*));
    if (graph->edges == NULL) {
        DestruccionDelGrafo(graph); //Destruir lo que se hizo hasta ahora
        return NULL;
    }
    graph->order = malloc(n_vertexes * sizeof(u32));
    if (graph->order == NULL) {
        DestruccionDelGrafo(graph); //Destruir lo que se hizo hasta ahora
        return NULL;
    }

    graph->delta = 0;
    return graph;
}

static Vertex*
graph_get_vertex(Grafo graph, u32 i) // From order array
{
    return graph->vertexes[graph->order[i]];
}

static u32
graph_get_vertex_index(Grafo graph, u32 i) // From order array
{
    return graph->order[i];
}

static Vertex*
graph_get_neighbor(Grafo graph, u32 i, u32 j)
{
    if (j >= graph->n_vertexes)
        return NULL;
    Vertex *v = graph_get_vertex(graph, i);
    int64_t index = list_get(v->neighbors, j);
    return (index == -1) ? NULL : graph->vertexes[index];
}

static u32
graph_binary_search_vertex(Grafo graph, u32 l, u32 r, u32 name)
{
    if (r >= l) {
        u32 mid = l + (r - l) / 2;
        if (graph->vertexes[mid]->name == name)
            return mid;
        if (graph->vertexes[mid]->name > name)
            return graph_binary_search_vertex(graph, l, mid - 1, name);
        return graph_binary_search_vertex(graph, mid + 1, r, name);
    }
    printf("Couldn't find vertex of name %d\n", name);
    exit(0);
}

static Edge*
graph_find_edge(Grafo graph, u32 i, u32 j)
{
    u32 v1, v2;
    v1 = graph_get_vertex_index(graph, i);
    v2 = graph_get_vertex_index(graph, j);
    for (u32 k = 0; k < graph->n_vertexes; k++) {
        Edge *e = graph->edges[i];
        if ((e->v1 == v1 && e->v2 == v2) || (e->v1 == v2 && e->v2 == v1))
            return e;
    }
    return NULL;
}

// Leer líneas del stdin:
struct line {
    char param;
    u32 a;
    u32 b;
};

static struct line
next_line(char *buf) //sirve para leer el archivo del grafo y retorna una line con a = vertex1 y b = vertex2
{
    struct line line;
    line.param = '\0';
    line.a = 0;
    line.b = 0;
    char ignoredStr[] = "edge";

    do {
        // carga el buffer con el contenido del standard input
        if (fgets(buf, BUFFER_SIZE, stdin) == NULL && !feof(stdin)) {
            printf("\tThere was a problem reading input\n");
            exit(0);
        }
        if (feof(stdin))
            return line;
    } while (isspace(buf[0])); //mientras halla un espacio al principio lo ignora
    line.param = buf[0]; // este seria e,c,p
    if (line.param == 'c') //no leer los que comienzen en c pues son comentarios
        return line; // Leer los valores ocasionaría un crasheo

    char temp[BUFFER_SIZE] = {0};
    char *end;
    u32 i = 0;
    u32 j = 0;

    // v1
    for (i = 1; i < BUFFER_SIZE; i++) {
        char c = buf[i];
        if (isspace(c)) //ignora los espacios
            continue;

        for (j = 0; j < BUFFER_SIZE; j++) {
            if (isspace(buf[i + j])) {
                temp[j] = '\0'; //NULL denota dónde termina el string
                break;
            }
            temp[j] = buf[i + j]; //carga en temp lo que lee hasta encontrar un espacio
        }

        if (!strcmp(temp, ignoredStr)) {
            i += j;
            memset(temp, '\0', BUFFER_SIZE);
            continue; // Si temp es el string a ignorar, seguir iterando
        }

        break;
    }
    errno = 0;
    line.a = strtol(temp, &end, 10);
    fail_if((line.a == 0 && errno != 0) || (*end != '\0'), "\tInvalid format: v1\n");

    //v2
    memset(temp, '\0', BUFFER_SIZE); //resetea el temporal
    for (i += j; i < BUFFER_SIZE; i++) {
        char c = buf[i];
        if (isspace(c))
            continue;

        for (j = 0; j < BUFFER_SIZE; j++) {
            if (isspace(buf[i + j])) {
                temp[j] = '\0';
                break;
            }
            temp[j] = buf[i + j];
        }
        break;
    }
    errno = 0;
    line.b = strtol(temp, &end, 10); //v2
    fail_if((line.b == 0 && errno != 0) || (*end != '\0'), "\tInvalid format: v2\n");
    return line;
}

int
comparator_edges_first(const void *p, const void *q)
{
    Edge *e = *(Edge**)p;
    Edge *_e = *(Edge**)q;
    if (e->v1 < _e->v1)
        return -1;
    else if (e->v1 > _e->v1)
        return 1;
    else {
        if (e->v2 < _e->v2)
            return -1;
        else if (e->v2 > _e->v2)
            return 1;
    }
    return 0; //Should not be reached
}

int
comparator_edges_second(const void *p, const void *q)
{
    Edge *e = *(Edge**)p;
    Edge *_e = *(Edge**)q;
    if (e->v2 < _e->v2)
        return -1;
    else if (e->v2 > _e->v2)
        return 1;
    else {
        if (e->v1 < _e->v1)
            return -1;
        else if (e->v1 > _e->v1)
            return 1;
    }
    return 0; //Should not be reached
}

int
comparator_vertexes(const void *p, const void *q)
{
    Vertex *v = *(Vertex**)p;
    Vertex *_v = *(Vertex**)q;
    if (v->name < _v->name)
        return -1;
    else if (v->name > _v->name)
        return 1;
    return 0;
}

// Grafo
Grafo
ConstruccionDelGrafo()
{
    // Init
    u32 n_vertexes = 0;
    u32 n_edges = 0;
    Grafo graph = NULL;

    // Helpers
    struct line line;
    char buf[BUFFER_SIZE];

    //printf("Reading input\n");

    for (;;) {
        line = next_line(buf); //lee la primera linea del archivo y segun su parametro es lo que hara con la inf
        switch (line.param) {
        case 'c': //no hacer nada pues c es una linea de comentario
            break;

        case 'p':
            //printf("\tInitting graph with %d vertexes and %d edges\n", line.a, line.b);
            n_vertexes = line.a;
            n_edges = line.b;

            fail_if(graph != NULL, "Can not init a graph twice\n");
            graph = graph_create(n_vertexes, n_edges * 2);
            break;

        case 'e':
            goto read_edges; // Salir del loop, se asume que ya se leyó p edge x y

        case '\0': // EOF inesperado
            //printf("Unexpeced EOF\n");
            exit(0);

        default:
            //printf("Invalid char: '%c'\n", line.param);
            exit(0);
        }
    }

    read_edges:
    fail_if(n_vertexes == 0 || n_edges == 0 || graph == NULL,
        "\t\tThere was a problem creating the graph\n");

    // Build edges
    //printf("\tDumping edges (1/5)\n");
    u32 edgeToAddIndex = 0;
    Edge *e;
    Edge *_e;

    for (u32 i = 0; i < n_edges; i++) {
        e = edge_create(line.a, line.b); // Store names
        _e = edge_create(line.b, line.a);
        e->sibling = _e;
        _e->sibling = e;
        graph->edges[edgeToAddIndex++] = e;
        graph->edges[edgeToAddIndex++] = _e;

        // Next line
        line = next_line(buf);
        if (line.param == '\0')
            break;
    }
    fail_if(edgeToAddIndex/2 != n_edges, "\t\tThere are too few edges\n");

    // Init vertexes and neighbors and set primary vertexes in edges
    //printf("\tSorting edges (2/5)\n");
    qsort(graph->edges, n_edges * 2, sizeof(Edge*), comparator_edges_first);

    //printf("\tMaking vertexes and setting order (3/5)\n");
    u32 vertexToAddIndex = 0;
    u32 prev = graph->edges[0]->v1 + 1; // To guarantee prev != graph->edges[0]->v1
    u32 isSame;
    for (u32 i = 0; i < n_edges * 2; i++) {
        e = graph->edges[i];
        isSame = prev == e->v1;
        prev = e->v1;
        if (!isSame) {
            //Setting order here to avoid having to do another loop
            graph->order[vertexToAddIndex] = vertexToAddIndex;
            graph->vertexes[vertexToAddIndex++] = vertex_create(e->v1);
        }
    }
    fail_if(vertexToAddIndex != n_vertexes, "\t\tThere are too few vertexes\n");

    // Set primary vertexes in edges
    //printf("\tSetting edges (4/5)\n");
    qsort(graph->vertexes, n_vertexes, sizeof(Vertex*), comparator_vertexes);
    u32 index = 0;
    prev = graph->edges[0]->v1 + 1; // To guarantee prev != graph->edges[0]->v1
    for (u32 i = 0; i < n_edges * 2; i++) {
        e = graph->edges[i];
        isSame = prev == e->v1;
        prev = e->v1;
        if (!isSame)
            index = graph_binary_search_vertex(graph, 0, n_vertexes - 1, e->v1);
        e->v1 = index; // v1 name to index
    }

    //printf("\tFinishing edges and linking neighbors (5/5)\n");
    qsort(graph->edges, n_edges * 2, sizeof(Edge*), comparator_edges_second);
    index = 0;
    prev = graph->edges[0]->v2 + 1; // To guarantee prev != graph->edges[0]->v2
    for (u32 i = 0; i < n_edges * 2; i++) {
        e = graph->edges[i];
        isSame = prev == e->v2;
        prev = e->v2;
        if (!isSame)
            index = graph_binary_search_vertex(graph, 0, n_vertexes - 1, e->v2);
        e->v2 = index; // v2 name to index

        Vertex *v1 = graph->vertexes[e->v1];
        vertex_add_neighbor(v1, index);
        if (v1->degree > graph->delta)
            graph->delta = v1->degree;
    }

    //printf("\tDone\n");
    return graph;
}

void
DestruccionDelGrafo(Grafo G)
{
    //printf("Destroying graph\n");
    if (G == NULL)
        return;
    if (G->vertexes != NULL) {
        for (u32 i = 0; i < G->n_vertexes; i++)
            vertex_destroy(G->vertexes[i]);
        free(G->vertexes);
    }
    if (G->edges != NULL) {
        for (u32 i = 0; i < G->n_edges; i++)
            edge_destroy(G->edges[i]);
        free(G->edges);
    }
    free(G->order);
    free(G);
    //printf("\tDone\n");
}

Grafo
CopiarGrafo(Grafo G)
{
    Grafo graph = graph_create(G->n_vertexes, G->n_edges);
    graph->delta = G->delta;
    if (graph == NULL)
        return NULL;
    for (u32 i = 0; i < G->n_vertexes; i++) {
        graph->vertexes[i] = vertex_copy(G->vertexes[i]);
        graph->order[i] = G->order[i];
    }
    for (u32 i = 0; i < G->n_edges; i++)
        graph->edges[i] = edge_copy(G->edges[i]);
    return graph;
}

//Get
u32
NumeroDeVertices(Grafo G)
{
    return G->n_vertexes;
}

u32
NumeroDeLados(Grafo G)
{
    return G->n_edges;
}

u32
Delta(Grafo G)
{
    return G->delta;
}

u32
Nombre(u32 i, Grafo G)
{
    return graph_get_vertex(G, i)->name;
}

u32
Color(u32 i, Grafo G)
{
    Vertex *v = graph_get_vertex(G, i);
    if (v == NULL)
        return -1;
    return v->color;
}

u32
Grado(u32 i, Grafo G)
{
    Vertex *v = graph_get_vertex(G, i);
    if (v == NULL)
        return -1;
    return v->degree;
}

u32
ColorVecino(u32 j, u32 i, Grafo G)
{
    Vertex *v = graph_get_neighbor(G, i, j);
    if (v == NULL)
        return -1;
    return v->color;
}

u32
NombreVecino(u32 j, u32 i, Grafo G)
{
    Vertex *v = graph_get_neighbor(G, i, j);
    if (v == NULL)
        return -1;
    return v->name;
}

u32
OrdenVecino(u32 j, u32 i, Grafo G)
{
    Vertex *v = graph_get_neighbor(G, i, j);
    if (v == NULL)
        return -1;
    for (u32 k = 0; k < G->n_vertexes; k++) {
        if (v == graph_get_vertex(G, k))
            return k;
    }
    return -1;
}

u32
PesoLadoConVecino(u32 j, u32 i, Grafo G)
{
    Edge *e = graph_find_edge(G, i, OrdenVecino(j, i, G));
    if (e == NULL)
        return -1;
    return e->weight;
}

// Set
char
FijarColor(u32 x, u32 i, Grafo G)
{
    if (i >= G->n_vertexes)
        return 1;

    Vertex *v = graph_get_vertex(G, i);
    if (v == NULL)
        return 1;
    v->color = x;
    return 0;
}

char
FijarOrden(u32 i, Grafo G, u32 N)
{
    if (N < G->n_vertexes && i < G->n_vertexes) {
        G->order[i] = N; // Los vértices ya están en el orden natural
        return 0;
    }
    return 1;
}

u32
FijarPesoLadoConVecino(u32 j, u32 i, u32 p, Grafo G)
{
    Edge *e = graph_find_edge(G, i, OrdenVecino(j, i, G));
    if (e == NULL)
        return 1;
    e->weight = p;
    return 0;
}

#ifndef LIST_H
#define LIST_H

#include <stdint.h>

typedef struct node {
    int data;
    struct node *next;
} Node;

typedef struct list {
    Node *head;
    Node *tail;
    Node *lastAccessed;
    uint32_t lastIndex;
} List;

List *list_create();
void list_add(List *list, uint32_t elem);
List *list_copy(List *list, uint32_t count);
void list_destroy(List *list);

uint32_t list_head(List *list);
uint32_t list_pop(List *list);
int64_t list_get(List *list, uint32_t index);

char list_is_empty(List *list);

#endif

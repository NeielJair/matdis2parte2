#ifndef UTIL_H
#define UTIL_H

void fail_if(char failCond, const char *msg);

int compare_u32(const void *a, const void *b);

#endif /* UTIL_H */

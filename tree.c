#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <errno.h>
#include "util.h"
#include "tree.h"

Tree
tree_create(uint32_t index)
{
    Tree tree = malloc(sizeof(struct branch));
    fail_if(tree == NULL, "There was a problem allocating a tree\n");
    tree->index = index;
    tree->data = list_create();
    tree->size = 0;
    tree->left = NULL;
    tree->right = NULL;
    return tree;
}

void
tree_add(Tree tree, uint32_t index, uint32_t elem)
{
    fail_if(tree == NULL, "Trying to add to a NULL tree\n");
    Tree iter = tree;
    for (;;) {
        if (iter->index == index) {
            list_add(iter->data, elem);
            iter->size++;
            break;
        } else if (iter->index < index) {
            if (iter->right == NULL)
                iter->right = tree_create(index);
            iter = iter->right;
        } else { // iter->index > index
            if (iter->left == NULL)
                iter->left = tree_create(index);
            iter = iter->left;
        }
    }
}

List*
tree_get(Tree tree, uint32_t index) {
    for (Tree iter = tree; iter != NULL;) {
        if (iter->index == index)
            return iter->data;
        iter = (iter->index < index) ? iter->right : iter->left;
    }
    return NULL;
}

void
tree_destroy(Tree tree)
{
    if (tree == NULL)
        return;
    list_destroy(tree->data);
    tree_destroy(tree->right);
    tree_destroy(tree->left);
    free(tree);
}

List*
tree_collapse(Tree tree, uint32_t *order, uint32_t orderSize) {
    List *list = list_create();
    for (uint32_t i = 0; i < orderSize; i++) {
        uint32_t index = order[i];
        for (Tree iter = tree; iter != NULL;) {
            if (iter->index == index) {
                if (!list_is_empty(tree->data)) {
                    // Append stored list to result
                    List *temp = list_copy(iter->data, iter->size);
                    if (list->head == NULL) {
                        free(list);
                        list = temp;
                    } else {
                        list->tail->next = temp->head;
                        list->tail = temp->tail;
                        free(temp);
                    }
                }
                break;
            }
            iter = (iter->index < index) ? iter->right : iter->left;
        }
    }
    return list;
}

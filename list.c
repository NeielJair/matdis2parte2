#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <string.h>
#include <errno.h>
#include "util.h"
#include "list.h"

List*
list_create()
{
    List *list = malloc(sizeof(List));
    list->head = NULL;
    list->tail = NULL;
    list->lastAccessed = NULL;
    list->lastIndex = 0;
    return list;
}

static Node*
node_create(uint32_t elem)
{
    Node *node = malloc(sizeof(Node));
    fail_if(node == NULL, "There was a problem allocating a node for a list\n");
    node->data = elem;
    node->next = NULL;
    return node;
}

void
list_add(List *list, uint32_t elem)
{
    if (list->head == NULL) {
        list->head = node_create(elem);
        list->tail = list->head;
        list->lastAccessed = list->head;
        list->lastIndex = 0;
    } else {
        list->tail->next = node_create(elem);
        list->tail = list->tail->next;
    }
}

List*
list_copy(List *list, uint32_t count)
{
    if (list->head == NULL || count <= 0)
        return NULL;

    List *new = list_create();
    Node *iter = list->head;
    list_add(new, iter->data);

    for (uint32_t i = 0; i < count - 1 && iter->next; i++) {
        list_add(new, iter->next->data);
        iter = iter->next;
    }

    return new;
}

void
list_destroy(List *list)
{
    Node *cur;
    Node *next;
    for (cur = list->head; cur != NULL;) {
        next = cur->next;
        free(cur);
        cur = next;
    }
    free(list);
}

uint32_t
list_head(List *list)
{
    return (list->head == NULL) ? 0 : list->head->data;
}

uint32_t
list_pop(List *list)
{
    if (list->head == NULL)
        return 0;

    Node *next = list->head->next;

    if (list->tail == list->head)
        list->tail = NULL;

    if (list->lastAccessed == list->head) {
        list->lastAccessed = next;
        list->lastIndex = 0;
    } else {
        if (list->lastIndex != 0)
            list->lastIndex--;
    }

    uint32_t ret = list->head->data;
    free(list->head);
    list->head = next;
    return ret;
}

int64_t
list_get(List *list, uint32_t index)
{
    if (list->lastAccessed != NULL) {
        if (list->lastIndex == index) {
            // lastAccessed is the node requested
            return list->lastAccessed->data;
        }
        if (list->lastIndex == index - 1 && list->lastAccessed->next != NULL) {
            // lastAccessed is the node previous to the one requested
            list->lastAccessed = list->lastAccessed->next;
            list->lastIndex++;
            return list->lastAccessed->data;
        }
    }
    // Look for the requested node
    Node *iter = list->head;
    for (uint32_t i = 0; i < index && iter != NULL; i++) {
        iter = iter->next;
    }

    if (iter == NULL)
        return -1;

    list->lastAccessed = iter;
    list->lastIndex = index;
    return iter->data;
}

char
list_is_empty(List *list) {
    return (list->head == NULL);
}

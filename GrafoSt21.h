#ifndef GRAFOST21_H
#define GRAFOST21_H

#include <stdint.h>

// Hecho por Neiel Urbano, Marcello Bussano y Eric Negreido.

typedef uint32_t u32;

typedef struct vertex {
    u32 name;
    u32 degree;
    u32 color;
    List *neighbors; // neighbors->data is the index of the vertex in graph->vertexes
} Vertex;

typedef struct edge {
    // Vertexes indexes in graph->vertexes
    u32 v1;
    u32 v2;
    // Weights
    u32 weight;
    struct edge *sibling;
} Edge;

typedef struct grafoSt {
    // Amounts
    u32 n_vertexes;
    u32 n_edges;
    // Lists
    Vertex **vertexes; // Ordered array by vertex->name
    Edge **edges;
    u32 *order; // Order of vertexes where vertexes[order[i]] is the i-th vertex
    // Δ
    u32 delta;
} GrafoSt;

#endif

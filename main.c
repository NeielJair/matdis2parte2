#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <errno.h>
#include "RomaVictor.h"
#include "GrafoSt21.h"
#include "UnleashHell.h"

#define max(a,b) \
  ({ __typeof__ (a) _a = (a); \
      __typeof__ (b) _b = (b); \
    _a > _b ? _a : _b; })

u32 greedyCounter = 0;

static u32
run_greedy(Grafo g)
{
    greedyCounter++;
    return Greedy(g);
}

static u32
evolve_random(Grafo g, u32 chi, u32 *perm, u32 seed)
{
    srand(seed);
    u32 offset = random_number(chi - 1);
    u32 coprime = get_random_coprime(chi);
    for (u32 i = 0; i < chi; i++)
        perm[i] = ((uint64_t)coprime * i + offset) % chi;

    OrdenPorBloqueDeColores(g, perm);
    u32 temp = chi;
    chi = run_greedy(g);
    if (chi > temp)
        printf("\t * The amount of colors increased, something's wrong *\n");
    return chi;
}

static u32
evolve_gtl(Grafo g, u32 chi, u32 *perm) // Greatest to lowest
{
    for (u32 i = 0; i < chi; i++)
        perm[i] = chi - i - 1;

    OrdenPorBloqueDeColores(g, perm);
    u32 temp = chi;
    chi = run_greedy(g);
    if (chi > temp)
        printf("\t * The amount of colors increased, something's wrong *\n");
    return chi;
}

static u32
evolve_gtl_random(Grafo g, u32 chi, u32 *perm, float probability)
{
    // init perm
    for (u32 i = 0; i < chi; i++)
        perm[i] = chi - i - 1;

    for (u32 i = 0; i < chi; i++) {
        float chance = random_number(1);
        if (chance <= probability) {
            // swap with random number
            u32 random = random_number(chi - 1);
            u32 temp = perm[i];
            perm[i] = perm[random];
            perm[random] = temp;
        }
    }

    OrdenPorBloqueDeColores(g, perm);
    u32 temp = chi;
    chi = run_greedy(g);
    if (chi > temp)
        printf("\t * The amount of colors increased, something's wrong *\n");
    return chi;
}

int
main(int argc, char **argv)
{
    /* 1 */
    // stdin = graph input
    // argv[0] = a = number of times to perform random orders
    // argv[1] = b = number of times to perform order by blocks
    // argv[2] = c = external cycles (copies)
    // argv[3] = d = internal cycles (tests on the copies)
    // argv[4] = e = used in the probability of swapping colors (1/e)
    // argv[5] = f = random seed
    if (argc < 7) {
        printf("Insufficient args, got %d\n", argc);
        exit(0);
    }
    u32 randomOrders = atoi(argv[1]); // a
    u32 blockOrders = atoi(argv[2]); // b
    u32 copyCycles = atoi(argv[3]); // c
    u32 testCycles = atoi(argv[4]); // d
    float probability; // e
    {
        float temp = atoi(argv[5]);
        probability = (temp == 0) ? 0 : 1/temp;
    }
    u32 seed = atoi(argv[6]); // f
    u32 initialSeed = seed;

    printf(
        "Params a=%d b=%d c=%d d=%d 1/e=%f f=%d\n",
        randomOrders, blockOrders, copyCycles, testCycles, probability, seed
    );

    /* 2 */
    // Load graph
    Grafo g = ConstruccionDelGrafo();
    printf("Graph loaded\n");
    printf("Number of vertexes: %d\n", NumeroDeVertices(g));
    printf("Number of edges: %d\n", NumeroDeLados(g));
    printf("Delta: %d\n", Delta(g));

    /* 3 */
    if (Bipartito(g)) {
        printf("Graph is bipartite\n");
        goto done;
    }
    printf("Graph is not bipartite\n");

    /* 5 */
    printf("Randomizing vertexes and running greedy:\n");
    u32 chi = run_greedy(g);
    u32 bestChi = chi;
    u32 bestSeed = -1;
    char bestSeedIsInitial = 1;

    for (u32 i = 0; i < randomOrders; i++) {
        AleatorizarVertices(g, seed);
        chi = run_greedy(g);
        if (chi < bestChi) {
            bestChi = chi;
            bestSeed = seed;
            bestSeedIsInitial = 0;
        }
        seed++;
        printf("\t[%d] Got %d colors\n", i, chi);
    }

    // Reset graph to the best iteration of greedy
    if (randomOrders != 0) {
        AleatorizarVertices(g, (bestSeedIsInitial) ? initialSeed : bestSeed);
        run_greedy(g);
    }

    /* 6 */
    printf("Ordering colors by random blocks and running greedy:\n");
    chi = bestChi;
    u32 *perm = malloc(chi * sizeof(u32));
    for (u32 i = 0; i < blockOrders; i++) {
        // Randomize colors order
        srand(seed + i);
        u32 offset = random_number(chi - 1);
        u32 coprime = get_random_coprime(chi);
        for (u32 j = 0; j < chi; j++) {
            perm[j] = ((uint64_t)coprime * j + offset) % chi;
        }

        OrdenPorBloqueDeColores(g, perm);

        u32 temp = chi;
        chi = run_greedy(g);
        if (chi > temp) {
            printf("\t * The amount of colors increased, something's wrong *\n");
            goto done;
        }
        printf("\t[%d] Got %d colors\n", i, chi);
    }

    /* 7 */
    Grafo copies[2];
    u32 bestChis[3];
    u32 chis[3] = {chi, chi, chi};

    bestChi = chi;
    perm = realloc(perm, chi * sizeof(u32));
    for (u32 i = 0; i < copyCycles; i++) {
        copies[0] = CopiarGrafo(g);
        copies[1] = CopiarGrafo(g);

        bestChis[0] = bestChi;
        bestChis[1] = bestChi;
        bestChis[2] = bestChi;

        for (u32 j = 0; j < testCycles; j++) {
            // (a)
            chis[0] = evolve_random(g, bestChis[0], perm, j);
            if (chis[0] < bestChis[0])
                bestChis[0] = chis[0];

            // (b)
            chis[1] = evolve_gtl(copies[0], bestChis[1], perm);
            if (chis[1] < bestChis[1])
                bestChis[1] = chis[1];

            // (c)
            chis[2] = evolve_gtl_random(copies[1], bestChis[2], perm, probability);
            if (chis[2] < bestChis[2])
                bestChis[2] = chis[2];

            printf("χs:\n");
            printf("\t(1) %d\n", chis[0]);
            printf("\t(2) %d\n", chis[1]);
            printf("\t(3) %d\n", chis[2]);
        }

        // result
        bestChi = bestChis[0];

        if (bestChis[1] < bestChis[0] && bestChis[1] <= bestChis[2]) {
            // copies[0] dethrones g
            bestChi = bestChis[1];
            Grafo temp = g;
            g = copies[0];
            copies[0] = temp;
        }
        if (bestChis[2] < bestChis[0] && bestChis[2] < bestChis[1]) {
            // copies[1] dethrones g
            bestChi = bestChis[2];
            Grafo temp = g;
            g = copies[1];
            copies[1] = temp;
        }
        DestruccionDelGrafo(copies[0]);
        DestruccionDelGrafo(copies[1]);
        printf("Best χ: %d\n\n", bestChi);
    }
    free(perm);

    done:
    printf("Done, performed %d greedies\n", greedyCounter);
    DestruccionDelGrafo(g);
    return 0;
}
